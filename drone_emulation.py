# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

from time import sleep
from clients.iot_agent import IotAgent
import random
from drone.subscribe import subscribe
from drone.db import get_all, get_by_id, get_last_n
from drone.utils import log_db_messages, log_iot_agent_messages

def create_device_data():
    device_data = []

    batch_size = 0
    while batch_size < 5:
        lon = str(random.uniform(-5, 5))
        lat = str(random.uniform(-5, 5))
        url = 'http://localhost'
        prediction = random.randint(0, 1)

        x1 = str(random.randint(250, 350))
        y1 = str(random.randint(250, 350))
        x2 = str(random.randint(250, 350))
        y2 = str(random.randint(250, 350))
        
        obj = {"location": f"{lat}, {lon}", "image_link": url, "prediction": [{"location": f"{lat}, {lon}", "disease_area": [{"x1": x1, "y1": y1}, {"x2": x2, "y2": y2}], "prediction": prediction}]}
        device_data.append(obj)

        batch_size += 1

    return device_data


def main():
    attributes_list = [{'name': 'device_data', 'type': 'StructuredValue'}]
    device = IotAgent(api_key="4jggokgpepnvsb2uv4s40d59ov", entity_type="Drone", resource="/iot/d", 
                      device_id="4", attributes=attributes_list, fiware_service="openiot", fiware_service_path="/")

    device.create_device()
    
    send = 0
    max_send_requests = 1

    print(f"Send data {max_send_requests} times for device urn:ngsi-ld:Device:{device.device_id} of type {device.entity_type}")
    
    while(send < max_send_requests):
        
        device_data = create_device_data()
        measurement = {"device_data": device_data}

        response = device.send_measurement(measurement)
        
        log_iot_agent_messages(response, send)
        
        send += 1

        #sleep(60)
    

if __name__ == "__main__":
    main()
    
    print("All the available data")
    print("----------------------")
    response = get_all()
    log_db_messages(response)
    
    print("The available data for device urn:ngsi-ld:Device:2")
    print("--------------------------------------------------")
    response = get_by_id("urn:ngsi-ld:Device:2")
    log_db_messages(response)

    print("The last 2 batches send by device urn:ngsi-ld:Device:2")
    print("--------------------------------------------------")
    response = get_last_n("urn:ngsi-ld:Device:2", 2)
    log_db_messages(response)
    