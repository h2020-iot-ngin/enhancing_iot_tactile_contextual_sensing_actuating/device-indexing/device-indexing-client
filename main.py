# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

from clients.orion import Orion
from clients.iot_agent import IotAgent
from helper.utils import log_measurement

def main():
    
    """
    Functions used to interact with Orion 
    """
    """
    # Create instance of Orion class to set the neccessary values
    orion = Orion(type="generic_device", id="7", alias="generating-device", ip="127.0.0.1", location="2.3, 4.3")

    # Create a new device
    print("Creating entity...")
    orion.create_entity()

    print("----------------------")

    # Get all available devices
    print("Loading all available entities...")
    orion.get_devices()

    print("----------------------")

    # Update the information of the entity
    print("Updating location of entity...")
    orion.location = "1.576768, -5.23729"
    orion.update()

    print("----------------------")

    # Get all available registrations
    print("Loading all available registrations...")
    orion.get_registrations()

    print("----------------------")

    # Get all available subscriptions
    print("Loading all available subscriptions...")
    orion.get_subcriptions()

    print("----------------------")

    # Send device for scanning and send scanning results in orion as properties of the device
    # For the demonstartion, we send a static example output of the results
    orion.vulnerabilities = ["web", "ssh"]
    print("Updating vulnerabilities of entity...")
    orion.scanning_results()
    """
    
    """
    Functions used to interact with IoT Agent
    """

    attributes_list = [ {'name': 'alias', 'type': 'Text'},
                        {'name': 'ip', 'type': 'Text'}, 
                        {'name': 'location', 'type': 'geo:point'},
                        {'name': 'vulnerabilities', 'type': 'StructuredValue'} ]

    # Create instance of IoTAgent class to set the neccessary values
    iot_agent = IotAgent(api_key="4jggokgpepnvsb2uv4s40d59ov1", entity_type="Drone", resource="/iot/d", device_id="10", 
                         attributes=attributes_list, fiware_service="openiot", fiware_service_path="/")

    # Create a new Service
    print("Creating service...")
    print("----------------------")
    iot_agent.create_service()

    print("----------------------")

    # Get all available Services
    print("Loading all available services...")
    print("---------------------------------")
    iot_agent.get_services()

    print("----------------------")
    
    # Get a particular Service's information
    print(f"Loading details of service for resource: {iot_agent.resource}")
    print("--------------------------------------------------------------")
    iot_agent.get_service_details()

    print("----------------------")

    # Create a new Device
    print("Creating device...")
    print("------------------")
    iot_agent.create_device()

    print("----------------------")

    # Get all available devices
    print("Loading all available devices...")
    print("--------------------------------")
    iot_agent.get_devices()

    print("----------------------")

    # Get a particular device's details
    print(f"Loading details of device: device{iot_agent.device_id}")
    print("------------------------------------------------------")
    iot_agent.get_device_details()
    
    print("----------------------")

    # Send an update to a device's attributes, through HTTP
    print("Updating device's attributes...")
    print("-------------------------------")
    measurement = {"location": "1.576768, -5.23729"}
    response = iot_agent.send_measurement(measurement)
    log_measurement(response)

    print("----------------------")

    # Get all available entities
    print("Loading all available entities...")
    print("---------------------------------")
    iot_agent.get_entities()

    print("----------------------")

    # Get a particular entity
    print(f"Loading details of entity: urn:ngsi-ld:Device:{iot_agent.device_id}")
    print("--------------------------------------------------------------------")
    iot_agent.get_entity()

    print("----------------------")
    
    # Get subscriptions for Service group
    print("Loading all available subscriptions...")
    print("--------------------------------------")
    iot_agent.get_subscriptions()

    print("----------------------")
    
    # Delete a Service
    print("Deleting service...")
    print("-------------------")
    iot_agent.delete_service()

    print("----------------------")

    # Delete a device
    print("Deleting device...")
    print("------------------")
    iot_agent.delete_device()

    print("----------------------")
    
    # Get a particular entity
    print(f"Deleting entity: urn:ngsi-ld:Device:{iot_agent.device_id}")
    print("----------------------------------------------------------")
    iot_agent.delete_entity()

if __name__ == "__main__":
    main()