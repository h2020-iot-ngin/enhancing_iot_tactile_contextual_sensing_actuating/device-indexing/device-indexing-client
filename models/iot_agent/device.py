# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os

class Device:
    def __init__(self, device_id: str, entity_type: str, attributes: list) -> None:
        self.device_id = f"device{device_id}"
        self.entity_name = f"urn:ngsi-ld:Device:{device_id}"
        self.entity_type = entity_type
        self.timezone = os.getenv('TIMEZONE')
        self.protocol = os.getenv('PROTOCOL')
        self.transport = os.getenv('TRANSPORT')
        self.attributes = attributes