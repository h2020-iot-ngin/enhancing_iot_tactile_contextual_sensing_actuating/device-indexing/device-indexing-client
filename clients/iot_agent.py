# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os
import json
import requests
from dataclasses import dataclass
from helper.utils import to_json, form_url, log_messages
from models.iot_agent.device import Device
from models.iot_agent.service import Service
from models.iot_agent.devices import Devices
from models.iot_agent.services import Services


@dataclass
class IotAgent:
    api_key: str
    entity_type: str
    resource: str
    device_id: str
    attributes: list
    fiware_service: str
    fiware_service_path: str

    def create_service(self):
        """
        Create a service group
        :param obj: the service to create
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the POST request
        """
        services = [{"apikey": self.api_key, "entity_type": self.entity_type, "resource": self.resource}]

        services = Services(services)
        create_service = to_json(services)

        headers = {
            "Content-Type": "application/json",
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        url = form_url("services")

        response = requests.post(url, data=create_service, headers=headers)

        log_messages(response)

    def get_services(self):
        """
        Get all available services
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the GET request
        """
        url = form_url("services")

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        response = requests.get(url, headers=headers)

        response = log_messages(response)

        return response

    def get_service_details(self):
        """
        Get a specific service by its resource
        :param resource: the endpoint the devices can use to authenticate themselves and send their measurements
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the GET request
        """
        url = form_url("services")
        url = f"{url}/?resource={self.resource}"

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }
        response = requests.get(url, headers=headers)

        response = log_messages(response)

        return response

    def update_service(self, service_update: dict):
        """
        Update a service (e.g. its entity_type)
        :param resource: the endpoint the devices can use to authenticate themselves and send their measurements
        :param api_key: the token used to authenticate a device
        :param service_update: a dictionary with the parameters of the service we want to update
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the PUT request
        """
        url = form_url("services")
        url = f"{url}/?resource={self.resource}&apikey={self.api_key}"

        headers = {
            "Content-Type": "application/json",
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }
        response = requests.put(url, data=json.dumps(service_update), headers=headers)

        log_messages(response)

    def delete_service(self):
        """
        Delete a service
        :param resource: the endpoint the devices can use to authenticate themselves and send their measurements
        :param api_key: the token used to authenticate a device
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the DELETE request
        """
        url = form_url("services")
        url = f"{url}/?resource={self.resource}&apikey={self.api_key}"

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }
        response = requests.delete(url, headers=headers)

        log_messages(response)

    def create_device(self):
        """
        Create a device
        :param obj: the device to create
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the POST request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)
        devices = [{"device_id": device.device_id, "entity_name": device.entity_name, "entity_type": self.entity_type, 
                "protocol": device.protocol, "transport": device.transport, "attributes": self.attributes}]
        
        devices = Devices(devices)
        create_device = to_json(devices)

        headers = {
            "Content-Type": "application/json",
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        url = form_url("devices")

        response = requests.post(url, data=create_device, headers=headers)

        log_messages(response) 

    def get_devices(self):
        """
        Get all available devices
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the GET request
        """
        url = form_url("devices")

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        response = requests.get(url, headers=headers)

        response = log_messages(response)

        return response

    def get_device_details(self):
        """
        Get a specific device by its device_id
        :return: the response of the GET request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)

        url = form_url("devices")
        url = f"{url}/{device.device_id}"

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }
        response = requests.get(url, headers=headers)

        response = log_messages(response)

        return response

    def update_device(self, device_update: dict):
        """
        Update a device (e.g. its entity_name)
        :param device_id: the device_id, exclusive for each device
        :param device_update: a dictionary with the parameters of the device we want to update
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the PUT request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)

        url = form_url("devices")
        url = f"{url}/{device.device_id}"

        headers = {
            "Content-Type": "application/json",
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }
        response = requests.put(url, data=json.dumps(device_update), headers=headers)

        log_messages(response)

    def delete_device(self):
        """
        Delete a device
        :param device_id: the device_id, exclusive for each device
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :param fiware_service_path: header used to differentiate between arrays of devices
        :return: the response of the DELETE request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)

        url = form_url("devices")
        url = f"{url}/{device.device_id}"

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }
        
        response = requests.delete(url, headers=headers)

        log_messages(response)

    def send_measurement(self, measurement: dict):
        """
        Send an update to the corresponding Orion entity through HTTP
        :param api_key: the token used to authenticate a device
        :param device_id: the device_id, exclusive for each device
        :param resource: the endpoint the devices can use to authenticate themselves and send their measurements
        :param measurement: a dict containing attributes-values pairs for update
        :return: the response of the POST request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)
        
        url = os.getenv('IOT_AGENT_NORTH_BOUND')
        url = f"{url}{self.resource}?k={self.api_key}&i={device.device_id}"

        headers = {
            "Content-Type": "application/json"
        }

        response = requests.post(url, data=json.dumps(measurement), headers=headers)

        return response

    def get_entities(self):
        """
        Get the entities created under a Fiware Service
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :return: the response of the GET request
        """
        url = form_url("entity")
        url = f"{url}?options=count"

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        response = requests.get(url, headers=headers)

        total_entities = int(response.headers['Fiware-Total-Count'])
        response = self.pagination(total_entities, 'entity')

        print(response)

        return response

    def get_entity(self):
        """
        Return a particular entity, by providing its entity_name
        :param entity_name: the field 'entity_name' for an Orion entity
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :return: the response of the GET request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)

        url = form_url("entity")
        url = f"{url}/{device.entity_name}"

        headers = {
            "Fiware-Service": self.fiware_service
        }

        response = requests.get(url, headers=headers)

        response = log_messages(response)
        return response

    def delete_entity(self):
        """
        Delete an entity, by providing its entity_name
        :param entity_name: the field 'entity_name' for an Orion entity
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :return: the response of the DELETE request
        """
        device = Device(self.device_id, self.entity_type, self.attributes)
        url = form_url("entity")
        url = f"{url}/{device.entity_name}?type={self.entity_type}"

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        response = requests.delete(url, headers=headers)

        log_messages(response)

    def get_subscriptions(self):
        """
        Get the subscriptions made under a Fiware Service
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :return: the response of the GET request
        """
        url = form_url("sub")
        url = f"{url}?options=count"

        headers = {
            "Fiware-Service": self.fiware_service
        }

        response = requests.get(url, headers=headers)

        total_entities = int(response.headers['Fiware-Total-Count'])
        response = self.pagination(total_entities, 'sub')

        print(response)

        return response

    def delete_subscription(self, subscription_id: str):
        """
        Delete a subscsription by providing its subscription_id
        :param subscription_id: the id of the subscription to delete
        :param fiware_service: header used for entities of a given service to be held in a separate database
        :return: the response of the DELETE request
        """
        url = form_url("sub")
        url = f"{url}/{subscription_id}"

        headers = {
            "Fiware-Service": self.fiware_service
        }

        response = requests.delete(url, headers=headers)

        log_messages(response)

    def pagination(self, total_entities: int, event: str):
        """
        Method to help with the retrieval of entities, 
        when they are large in number
        :param total_entities: the total number of the entities stored in Orion
        :param event: whether the URL has to do with entities, registrations or subscriptions
        :return: a list of all the entities
        """
        # https://fiware-orion.readthedocs.io/en/master/user/pagination/index.html
        counter = 0
        offset = 0
        limit = int(os.getenv('ENTITIES_LIMIT'))
        data = []

        headers = {
            "Fiware-Service": self.fiware_service,
            "Fiware-ServicePath": self.fiware_service_path
        }

        while offset <= total_entities:
            offset = limit * counter
            url = form_url(event)
            url = f"{url}?offset={offset}&limit={limit}"
            response = requests.get(url, headers=headers)
            data.append(response.json())
            counter += 1
        
        # https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-a-list-of-lists
        data = [item for sublist in data for item in sublist]

        return data
