# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os
import json
import requests
from dataclasses import dataclass
from models.orion.entity import Entity
from models.orion.register import Register
from models.orion.subscribe import Subscribe
from models.orion.update import Update
from helper.utils import to_json, form_url, log_messages

@dataclass
class Orion:
    type: str
    id : str
    alias: str
    ip: str
    location: str
    vulnerabilities = []

    def create_entity(self):
        """
        Create on Orion a new entity for the device
        :param obj : the device we want to reprsent as entity on orion
        :return : the response of the POST request
        """
        id = f"urn:ngsi-ld:Device:{self.id}"
        alias = {"value": self.alias}
        ip = {"value": self.ip}
        location = {"value": self.location, "type": "geo:point"} # device_location is of the form "2.3, 3.4"

        vulnerabilities = {"value": self.vulnerabilities}

        entity = Entity(self.type, id, alias, ip, location, vulnerabilities)
        request = to_json(entity)

        header = {"Content-Type": "application/json"}
        url = form_url("entity")

        response = requests.post(url, data=request, headers=header)

        log_messages(response)

    def get_devices(self):
        """
        Return all the entities stored in Orion
        :param: None
        :return: the response of the GET request
        """
        url = form_url("entity")
        url = f"{url}?options=count"
        response = requests.get(url)
        # Get the total number of entities from the header of the response
        total_entities = int(response.headers['Fiware-Total-Count'])
        response = self.pagination(total_entities, 'entity')
        
        print(response)

        return response

    def register(self):
        """
        Register device as a Context Provider
        :param obj: the device to become a Context Provider
        :return: the response of the POST request
        """
        id = f"urn:ngsi-ld:Device:{self.id}"
        entities = {"entities": [{"id": id, "type": self.type}]} # the device that will work as our Context Provider
        attrs = {"attrs": ("ip", "alias", "location")} # the values it will provide info for
        http = {"http": {"url": f"http://{self.ip}"}} # the device provides the info through this URL
        legacyForwarding = {"legacyForwarding": True} 

        des = f"Registration for device: {id}"
        dataProvided = {**entities, **attrs}
        provider = {**http, **legacyForwarding}

        reg = Register(des, dataProvided, provider)
        register = to_json(reg)

        header = {"Content-Type": "application/json"}
        url = form_url("reg")

        response = requests.post(url, data=register, headers=header)

        log_messages(response)

    def get_registrations(self):
        """
        Return all the registered devices
        :param: None
        :return: the response of the GET request
        """
        url = form_url("reg")
        url = f"{url}?options=count"
        response = requests.get(url)
        total_entities = int(response.headers['Fiware-Total-Count'])
        response = self.pagination(total_entities, 'reg')
        
        print(response)

        return response

    def subscribe(self):
        """
        A Device Controller subscribes to a device to receive notifications for changes in its attributes' values
        :param obj: the device the controller wants to subscribe to
        :parma controller: the device controller
        :return : the response of the POST request
        """
        id = f"urn:ngsi-ld:Device:{self.id}"
        entities = {"entities": [{"id": id, "type": self.type}]}
        condition = {"condition": {"attrs": ["ip", "location", "vulnerabilities"]}} # triggers for notification
        not_url = {"http": {"url": os.getenv('DEVICE_MANAGER')}} # where to send the notification
        not_attrs = {"attrs": ("alias", "ip", "location", "vulnerabilities")} # the contents of the notification
        attrs_format = {"attrsFormat" : "keyValues"}

        des = f"Subscription of device manager for device: {id}"
        subject = {**entities, **condition}
        notification = {**not_url, **not_attrs, **attrs_format}

        sub = Subscribe(des, subject, notification)
        subscribe = to_json(sub)

        header = {"Content-Type": "application/json"}
        url = form_url("sub")
        response = requests.post(url, data=subscribe, headers=header)

        log_messages(response)

    def get_subcriptions(self):
        """
        Return all the subscriptions
        :param: None
        :return: the response of the GET request
        """
        url = form_url("sub")
        url = f"{url}?options=count"
        response = requests.get(url)
        total_entities = int(response.headers['Fiware-Total-Count'])
        response = self.pagination(total_entities, 'sub')
        
        print(response)

        return response

    def update(self):
        """
        Update values of device's attributes
        :param obj : the device to have its attributes' values updated
        :return: the response of the POST request
        """
        id = f"urn:ngsi-ld:Device:{self.id}"
        actionType = "update"
        entities = [{"type": self.type, "id": id, "alias": {"value": self.alias}, "ip": {"value": self.ip}, "location": {"value": self.location, "type": "geo:point"}}]

        update= Update(actionType, entities)
        request = to_json(update)

        header = {"Content-Type": "application/json"}
        url = form_url("update")

        response = requests.post(url, data=request, headers=header)

        log_messages(response)

    def scanning_results(self):
        """
        Append the results of the scanning process to the scanned device's attributes
        :param obj: the device which has been scanned
        :return: the response of the POST request 
        """
        id = f"urn:ngsi-ld:Device:{self.id}"
        actionType = "update"
        entities = [{"type": self.type, "id": id, "vulnerabilities": {"value": self.vulnerabilities}}]

        update= Update(actionType, entities)
        request = to_json(update)

        header = {"Content-Type": "application/json"}
        url = form_url("update")

        response = requests.post(url, data=request, headers=header)

        log_messages(response)

    def delete(self, event: str, event_id=None):
        """
        Delete an entity, regsitration or subscription
        :param event: the type of the object we want to delete
        :param event_id: to delete a subscription or registration, provide its id
        :return: the response of the DELETE request
        """
        if event_id == None:
            id = f"urn:ngsi-ld:Device:{self.id}"
            print(id)
            url = form_url(event)
            url = f"{url}/{id}?type={self.type}"

            response = requests.delete(url)
        else:
            print(event_id)
            url = form_url(event)
            url = f"{url}/{event_id}?type={self.type}"

            response = requests.delete(url)

        log_messages(response)

    def pagination(self, total_entities: int, event: str):
        """
        Method to help with the retrieval of entities, 
        when they are large in number
        :param total_entities: the total number of the entities stored in Orion
        :param event: whether the URL has to do with entities, registrations or subscriptions
        :return: a list of all the entities
        """
        # https://fiware-orion.readthedocs.io/en/master/user/pagination/index.html
        counter = 0
        offset = 0
        limit = int(os.getenv('ENTITIES_LIMIT'))
        data = []

        while offset <= total_entities:
            offset = limit * counter
            url = form_url(event)
            url = f"{url}?offset={offset}&limit={limit}"
            response = requests.get(url)
            data.append(response.json())
            counter += 1
        
        # https://stackoverflow.com/questions/952914/how-to-make-a-flat-list-out-of-a-list-of-lists
        data = [item for sublist in data for item in sublist]

        return data