# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os
import json

def to_json(obj):
    """
    Turn body of request to JSON format
    :param obj: the body of the request
    :return: JSON formatted body
    """
    request = json.dumps(obj.__dict__, indent=3)
    return request

def form_url(event: str):
    """
    Form the URL for the requests
    :param event: whether the URL has to do with entities, registrations or subscriptions
    :return: the appropriate URL
    """
    if event == "reg":
        url = os.getenv('ORION_URL')
        url = f"{url}/v2/registrations"
    elif event == "sub":
        url = os.getenv('ORION_URL')
        url = f"{url}/v2/subscriptions"
    elif event == "entity":
        url = os.getenv('ORION_URL')
        url = f"{url}/v2/entities"
    elif event == "update":
        url = os.getenv('ORION_URL')
        url = f"{url}/v2/op/update"
    elif event == "services":
        url = os.getenv('IOT_AGENT_URL')
        url = f"{url}/iot/services"
    elif event == "devices":
        url = os.getenv('IOT_AGENT_URL')
        url = f"{url}/iot/devices"

    return url    

def log_messages(response):
    """
    Log messages for the requests
    """
    if response.status_code == 200:
        print(response.text)
        return response.json()
    elif response.status_code == 201:
        print("The request was successful and the entity/service/device was created")
    elif response.status_code == 204:
        print("The request was successful and the entity/service/device was updated/deleted")
    else:
        print(f"An error occured with status code: {response.status_code} and message: {response.text}")
    

def log_measurement(response):
    """
    Log messages for when sending measurements to IoT Agent through HTTP
    """
    if response.status_code == 200:
        print(f"Measurement was received successfully!")
    else:
        print(f"An error occured with status code: {response.status_code} and message: {response.text}")

