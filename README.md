# Orion Client

## Purpose
The purpose of this client is to offer an easy method to interact with the Orion Context Broker and the IoT Agent. 

## Installation and Execution
In order to install all the necessary dependencies, run:
```bash
pipenv install
```

In order to execute the code, run:
```bash
pipenv run python3 main.py
```


It is also necessary to create a `.env` file and include the parameters below:
```python
ORION_URL=???
IOT_AGENT_URL=???
ENTITIES_LIMIT=???
TIMEZONE=???
PROTOCOL=???
TRANSPORT=???
IOT_AGENT_NORTH_BOUND=???
```

The following table presents the role of each value and provides an example value:

| Environmental Variable | Role | Example Value | 
| ---                    | ---  |     ---       |
| ORION_URL | The URL Orion can  be accessed through | http://localhost:1026 |
| IOT_AGENT_URL | The URL IoT Agent can be accesed through for API requests | http://localhost:4041 |
| ENTITIES_LIMIT | Set `limit` variable, required for [pagination mechanism](https://fiware-orion.readthedocs.io/en/master/user/pagination/index.html) | 10 |
| TIMEZONE | The `Timezone` parameter, used when creating an IoT Device | Europe/Greece |
| PROTOCOL | The `Protocol` parameter, used when creating an IoT Device | IoTA-JSON |
| TRANSPORT | The transport protocol the physical device will use to communicate | HTTP |
| IOT_AGENT_NORTH_BOUND | The URL IoT Agent listens to for incoming messages by physical devices, through HTTP | http://localhost:7896 |

The `.env` file for the Onelab installation is the following:
```bash
ORION_URL=https://orion.apps.iot-ngin.onelab.eu
IOT_AGENT_URL=http://apps.iot-ngin.onelab.eu:30481
ENTITIES_LIMIT=10
TIMEZONE=Europe/Greece
PROTOCOL=IoTA-JSON
TRANSPORT=MQTT
IOT_AGENT_NORTH_BOUND=http://apps.iot-ngin.onelab.eu:32333
DEVICE_REGISTRY=https://historic-data-registry.iot-ngin.onelab.eu
```

## Structure
* The `main.py` file is used to execute the functions for the API calls.
The contents of the file can be altered as one desires, as it was constructed to demonstrate the usage of the functions.

* The `clients` folder includes separate clients for Orion and IoT Agent, to set the necessary values and make the appropriate calls for interacting with the components.

* The `helper` folder includes a file which holds functions used by both clients.

* The `models` folder holds the models required to construct the necessary objects for the API calls. More information about the available API calls and the necessary requirements can be found here:

    * [Orion Context Broker API](https://swagger.lab.fiware.org/?url=https://raw.githubusercontent.com/Fiware/specifications/master/OpenAPI/ngsiv2/ngsiv2-openapi.json#/Batch%20Operations/Update)
    * [IoT Agent API](https://swagger.lab.fiware.org/?url=https://raw.githubusercontent.com/FIWARE/specifications/master/OpenAPI/iot.IoTagent-node-lib/IoTagent-node-lib-openapi.json#/)

## Example usage 
In this segment, a few basic examples of how to use the functions will be shown: 

# Orion Context Broker
* Create an Orion Client Instance 
```python
from clients.orion import Orion

# Create instance of Orion class to set the neccessary values
orion = Orion(type="generic_device", id="7", alias="generating-device", ip="127.0.0.1", location="2.3, 4.3")
```

* Basic API calls
```python
# Create an Orion Context Broker entity
print("Creating entity...")
orion.create_entity()

print("----------------------")

# Get all available devices
print("Loading all available entities...")
orion.get_devices()

print("----------------------")

# Update device's information (alias, IP, location)
print("Updating location of entity...")
orion.location = "1.576768, -5.23729"
orion.update()

print("----------------------")

# Create registration for device
print("Creating registration...")
orion.register()

print("----------------------")

# Check all available registrations
print("Loading all available registrations...")
orion.get_registrations()

print("----------------------")

# Get all available subscriptions
print("Loading all available subscriptions...")
orion.get_subcriptions()

print("----------------------")

# Delete an entity, registration or subscription
print("Deleting subscription...")
orion.delete("sub")

print("----------------------")

# Update an entity's vulnerabilities, after scanning
orion.vulnerabilities = ["web", "ssh"]
print("Updating vulnerabilities of entity...")
orion.scanning_results()  
```

# IoT Agent
* Create an Iot Agent Client Instance 
```python
from clients.iot_agent import IotAgent

attributes_list = [ {'name': 'alias', 'type': 'Text'},
                        {'name': 'ip', 'type': 'Text'}, 
                        {'name': 'location', 'type': 'geo:point'},
                        {'name': 'vulnerabilities', 'type': 'StructuredValue'} ]

# Create instance of IoTAgent class to set the neccessary values
iot_agent = IotAgent(api_key="4jggokgpepnvsb2uv4s40d59ov1", entity_type="Drone", resource="/iot/d", device_id="10", 
                        attributes=attributes_list, fiware_service="openiot", fiware_service_path="/")
```

* Basic API calls for a Service
```python
# Create a new Service
print("Creating service...")
iot_agent.create_service()

print("----------------------")

 # Get all available Services
print("Loading all available services...")
iot_agent.get_services()

print("----------------------")

# Get a particular Service's information
print(f"Loading details of service for resource: {iot_agent.resource}")
iot_agent.get_service_details()

print("----------------------")

# Update a Service's information 
print("Updating service details...")
update = {"entity_type": "IoT-Device"}
orion.update_service(update)
print("----------------------")

# Delete a Service
print("Deleting device...")
iot_agent.delete_device()
```

* Basic API calls for a Device
```python
from helper.utils import log_measurement

# Create a new Device
print("Creating device...")
iot_agent.create_device()

print("----------------------")

# Get all available devices
print("Loading all available devices...")
iot_agent.get_devices()

print("----------------------")

# Get a particular device's details
print(f"Loading details of device: device{iot_agent.device_id}")
iot_agent.get_device_details()

print("----------------------")

# Update a device's information
update = {"entity_name": "urn:ngsi-ld:Thing:bell002"}
response = iot_agent.update_device(update)

print("----------------------")

# Delete a device
print("Deleting device...")
iot_agent.delete_device()

print("----------------------")

# Send an update to a device's attributes, through HTTP
print("Updating device's attributes...")
measurement = {"location": "1.576768, -5.23729"}
response = iot_agent.send_measurement(measurement)
log_measurement(response)
```

Special Orion API calls, for entities created through the IoT agent
```python 
import helper.iot_agent.utils as iot_agent_utils

# Get a particular entity
print(f"Loading details of entity: urn:ngsi-ld:Device:{iot_agent.device_id}")
iot_agent.get_entity()

print("----------------------")

# Get all available entities
print("Loading all available entities...")
iot_agent.get_entities()

print("----------------------")

# Get a particular entity
print(f"Deleting entity: urn:ngsi-ld:Device:{iot_agent.device_id}")
iot_agent.delete_entity()

print("----------------------")

# Get subscriptions for Service group
print("Loading all available subscriptions...")
iot_agent.get_subscriptions()
```

# Drone Emulation

## Purpose 
The Drone Emulation is used to recreate the basic workflow of a drone, meaning creating the data and sending them through the IoT Agent to Orion Context Broker.

## Execution
In order to execute the code, run:
```bash
pipenv run python3 drone_emulation.py
```

# Synfield Emulation

## Purpose
The Synfield Emulation is used to recreate the basic functionality of a Synfield device, meaning gathering the appropriate measurements and sending them through the IoT Agent to Orion Context Broker.

## Execution
In order to execute the code, run:
```bash
pipenv run python3 synfield.py
```

# Device Registry
Since Orion does not offer the ability to store historic data (only the latest update is preserved), a Device Registry is subscribed to Orion to receive notifications and keep a record of all the objects sent by the drone.

In order to interact with the registry, there are a few endpoints available, presented in the following table: 

| Endpoint | HTTP Request | Explanation |
|   ---    |     ---      |     ---     |
| `/get_all_records` | `GET` | Get all the available records |
| `/get_records/<entity_id>` | `GET` | Get all the records which correspond to a particular entity id |
| `/notification` | `POST` | Receive notification sent by Orion Context Broker |
| `/get_last_n_records/<entity_id>?last_n=<last_n>`  | `GET` | Get the last n records stored in mongodb | 

It is important to add in `.env` file the following environmental variables: 

| Environmental Variable | Role | Example Value | 
| ---                    | ---  |     ---       |
| DEVICE_REGISTRY | The URL the Device Registry can be accessed from | http://localhost |

The functions used to implement the API calls can be found in `drone/db.py` file.