import json
import random
from time import sleep
from clients.iot_agent import IotAgent
from helper.utils import log_measurement

def create_data():
    # lon = str(random.uniform(-5, 5))
    # lat = str(random.uniform(-5, 5))

    # Give a standard location (lat, lon) or uncomment the 2 lines bove to create random values
    lat = 38.0270852
    lon = 23.7329405

    # Open the template for the Synfield data
    sensors = open("./synfield.json")
    data = json.load(sensors)

    # Give random values for each Synfield measurement
    temperature = str(random.uniform(18, 27))
    air_humidity = str(random.uniform(40, 70))
    soil_moisture = str(random.uniform(35, 45))
    wind_speed = str(random.uniform(0, 20))
    rain = str(random.uniform(0, 21))
    battery = str(random.uniform(3.5, 3.8))
    system_temperature = str(random.uniform(35, 40))
    current = str(random.uniform(0, 142))

    # Pass the new values in the Synfield JSON
    for obj in data:
        if obj['sensor_id'] == 1:
            obj['measurement'] = temperature
        elif obj['sensor_id'] == 2:
            obj['measurement'] = air_humidity
        elif obj['sensor_id'] == 3:
            obj['measurement'] = soil_moisture
        elif obj['sensor_id'] == 5:
            obj['measurement'] = wind_speed
        elif obj['sensor_id'] == 7:
            obj['measurement'] = rain
        elif obj['sensor_id'] == 8:
            obj['measurement'] = battery
        elif obj['sensor_id'] == 12:
            obj['measurement'] = system_temperature
        elif obj['sensor_id'] == 24:
            obj['measurement'] = current

    # Create the final Synfield measurement to send to IoT Agent
    synfield_measurement = {"location": f"{lon}, {lat}", "sensors": f"{data}"}
    print(synfield_measurement)

    return synfield_measurement


def main():
    # The attributes that describe a Synfield device
    attributes_list = [{'name': 'location', 'type': 'geo:point'},
                       {'name': 'sensors', 'type': 'StructuredValue'}]

    # The mandatory headers for the request to IoT Agent (for more information see: https://gitlab.com/h2020-iot-ngin/enhancing_iot_tactile_contextual_sensing_actuating/device-indexing/device-indexing-readme#fiware-iot-agent)
    fiware_service="demo"
    fiware_service_path="/synfield"
    
    # Create an IoT Agent instance for the Synfield device
    device_1 = IotAgent(api_key="4jggokgpepnvsb2uv4s40v13", entity_type="Synfield", resource="/iot/d", 
                      device_id="8394121677753883", attributes=attributes_list, fiware_service=fiware_service, fiware_service_path=fiware_service_path)


    # If you run the script for the first time, execute `device_1.create_service()` and `device_1.create_device()`, otherwise put in comments
    # Create service
    device_1.create_service()

    # Create device
    device_1.create_device()

    # Create the measurent for the synfield device
    measurement_1 = create_data()

    # Send the measurement to Orion
    response_1 = device_1.send_measurement(measurement_1)
    
    # Log the response
    log_measurement(response_1)

    # Return the Digital Twin of the device on Orion
    device_1.get_entities()


if __name__ == "__main__":
    create_data()
    