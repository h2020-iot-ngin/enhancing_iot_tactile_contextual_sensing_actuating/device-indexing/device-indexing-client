# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os
import json
import requests
from models.orion.subscribe import Subscribe

DB_URL="http://device-indexing-registry.iot-ngin-wp4:80/notification"

def subscribe():
    """
    The Device Manager subscribes to receive notifications for all the devices
    :param: None
    :return: the response of the POST request
    """
    manager_ip = DB_URL
    
    entities = {"entities": [{"idPattern": ".*", "typePattern": ".*"}]} # receive notifications for all devices, regardless its id or type
    condition = {"condition": {"attrs": ["device_data"]}} # triggers for notification
    not_url = {"http": {"url": manager_ip}} # where to send the notification
    not_attrs = {"attrs": ["device_data"]} # the contents of the notification
    attrs_format = {"attrsFormat" : "keyValues"} # the attributes will have key-value format

    des = "Subscription of device manager for devices"
    subject = {**entities, **condition}
    notification = {**not_url, **not_attrs, **attrs_format}

    sub = Subscribe(des, subject, notification)
    subscribe = to_json(sub)

    headers = {
        "Content-Type": "application/json",
        "Fiware-Service": "openiot"
    }
    url = form_url()

    response = requests.post(url, data=subscribe, headers=headers)

    return response

def to_json(obj):
    """
    Turn body of request to JSON format
    :param obj: the body of the request
    :return: JSON formatted body
    """
    request = json.dumps(obj.__dict__, indent=3)
    return request

def form_url():
    """
    Form the URL for the requests
    :param: None
    :return: the appropriate URL
    """
    url = os.getenv('ORION_URL')
    url = f"{url}/v2/subscriptions"