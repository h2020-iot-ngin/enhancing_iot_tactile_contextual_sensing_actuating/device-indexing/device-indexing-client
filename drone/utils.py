# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

def log_iot_agent_messages(response, counter):
    if response.status_code == 200:
        print(f"Batch {counter} was received successfully!")
    else:
        print(f"An error occured with status code: {response.status_code} and message: {response.text}")

def log_db_messages(response):
    if response.status_code == 200:
        print(response.text)
    else:
        print(f"An error occured with status code: {response.status_code} and message: {response.text}")