# Disclaimer
# ----------
#
# Copyright (C) 2022 Synelixis Solutions SA
#
# This file is part of Device Indexing Client.
#
# Device Indexing Client is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Device Indexing Client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
# General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Device Indexing Client. If not, see http://www.gnu.org/licenses/.
#
# For those usages not covered by this license please contact with 
# info at synelixis dot com
#

import os
import requests

def get_all():
    url = os.getenv('DEVICE_REGISTRY')
    url = f"{url}/get_all_records"

    headers = {
        "Content-Type": "application/json",
        "Fiware-Service": "openiot",
        "Fiware-ServicePath": "/"
    }

    response = requests.get(url, headers=headers)

    return response

def get_by_id(id):
    url = os.getenv('DEVICE_REGISTRY')
    url = f"{url}/get_records/{id}"

    headers = {
        "Content-Type": "application/json",
        "Fiware-Service": "openiot",
        "Fiware-ServicePath": "/"
    }

    response = requests.get(url, headers=headers)

    return response

def get_last_n(id, last_n):
    url = os.getenv('DEVICE_REGISTRY')
    url = f"{url}/get_last_n_records/{id}?last_n={last_n}"

    headers = {
        "Content-Type": "application/json",
        "Fiware-Service": "openiot",
        "Fiware-ServicePath": "/"
    }

    response = requests.get(url, headers=headers)

    return response
